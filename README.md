# :warning: お知らせ

**NeeLaboratory.IO.Search プロジェクトページは [GitHub](https://github.com/neelabo/NeeLaboratory.IO.Search) に移動しました。**  
**ここのリポジトリは今後更新されません。2025年6月以後に削除されます。**

----

# NeeLaboratory.IO.Search

RealtimeSearch から検索処理部分をライブラリ化したもの。